FROM golang:1.21-alpine AS build
WORKDIR /app

COPY go.* .
RUN go mod download

COPY . .
RUN go test ./...
RUN go build src/main.go

FROM alpine:3.14
WORKDIR /app

COPY --from=build /app/main .

ENTRYPOINT ["./main"]
