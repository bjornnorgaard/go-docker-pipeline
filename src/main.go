package main

import (
	"fmt"
	"gitlab.com/bjornnorgaard/go-docker-pipeline/src/calculator"
)

func main() {
	result := calculator.Add(1, 2)
	fmt.Println("Hello World", result)
}
