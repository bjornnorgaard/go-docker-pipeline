package calculator

import "testing"

func TestAdd(t *testing.T) {
	result := Add(1, 2)
	if result != 3 {
		t.Errorf("Add(1,2) failed, expected %v, got %v", 3, result)
	}
}
